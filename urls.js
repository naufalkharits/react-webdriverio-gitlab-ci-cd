module.exports = {
    local: "http://localhost:3000/",
    prod: "https://naufal-test-prod.herokuapp.com/",
    qa: "https://naufal-qa.herokuapp.com/",
};
