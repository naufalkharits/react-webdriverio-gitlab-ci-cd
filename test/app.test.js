describe("React application home page", () => {
    it('Verify that the app link says Learn React"', async () => {
        browser.url("/");
        let text = $(".App-link").getText();
        assert.equal(text, "Learn React");
    });
});
